\section{Background}

\subsection{Assumptions}
We assume that everybody is broadly familiar with the most basic ideas
of electricity, in particular:

That \term{current} --- measured in amperes\footnote{Although named
  for Amp\`ere, the SI unit does not bear an accent.}, symbol~\unit{A}
--- is the flow rate of electric \term{charge} --- measured in
coulombs, symbol~\unit{C}.
\[
1~\unit{A} = 1~\unit{C}{s}
\]

That there are two kinds of charge, \emph{positive} and
\emph{negative}, that like charges repel each other and unlike charges
attract each other according to \term{Coulomb's Law}: an
inverse-square force law:
\[
\vec{F}_\mathrm{C} = \frac{Q_1 Q_2}{4\pi\epsilon\abs{\vec{r}_{12}}^2}\hat{r}_{12}
\]

That the predominant --- for our purposes exclusive --- charge-carrier
is the negatively-charged \term{electron}, which carries a charge of
$\approx-1.6\times 10^{-19}~\unit{C}$, the magnitude of which is called the
\term{elementary charge} and denoted\footnote{To avoid confusion with
  Euler's number, $\mathrm{e}$, we will use $q$.} $e$ or $q$.

That, for historical reasons, \term{conventional current} is
conceptualized as the flow of \emph{positive} charge but, in reality,
negatively charged electrons are flowing in the opposite direction.

That this is arbitrary and immaterial to circuit analysis.

That \term{electric potential difference}, or just \term{potential
  difference}, colloquially ``\term{voltage}'' is a measure of the
difference in potential energy (in joules) per unit charge (in
coulombs) between two points; the unit, the \term{volt},
symbol~\unit{V}, is a joule-per-coulomb.
\[
1~\unit{V} = 1~\unit{J}{C}
\]

That electric fields and currents obey the principle of linear
superposition.

That different materials impede the flow of electrons to different
degrees. The property that quantifies this effect between two points
on an object is \term{resistance}, measured in ohms, symbol~\unit{\Omega}.

That the bulk material property that quantifies this effect is
\emph{resistivity}, measured in ohm-meters, symbol~\unit{\Omega\cdot m}.

That the relationship between the potential difference between two
points, the resistance between those points, and the current that
flows from one point to the other is given by \term{Ohm's Law}:
\begin{equation}
\label{eqn:vir}
V = I\times R
\end{equation}

\begin{figure}[ht]
  \centering
  \begin{circuitikz}
    \draw (0,0) to [short, i=$I$] (2,0);
    \draw (2,0) to [R=$R$, v=$V$] (4,0);
    \draw (4,0) to [short] (6,0);
  \end{circuitikz}
  \caption{Resistor Labeled with Current and Voltage}
  \label{fig:ohmslaw}
\end{figure}


That the reciprocal of resistance is \term{conductance} and has units
of siemens, symbol \unit{S}.

That the reciprocal of resistivity is \term{conductivity} and has
units of siemens per meter, symbol \unit{S}{m}.

That (DC) electric power consumed by a resistance is the product of
potential difference and current:
\[
P = I\times V = I^2\times R = \frac{V^2}{R}
\]
