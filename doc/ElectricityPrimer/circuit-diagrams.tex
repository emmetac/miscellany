\subsection{Circuit Diagrams}

Having introduced the zig-zag circuit symbol for a resistor in
Figure~\ref{fig:ohmslaw}, it may be helpful to briefly mention
\term{circuit diagrams} or \term{schematics}, which depict circuit
elements or components and how they are connected to each other.

Section~\ref{sec:circuit-elements}, below, contains a reasonably
comprehensive description of the most common circuit symbols. At this
stage, it's useful to know a little about circuit diagrams in general,
even if we can't discuss some important circuit symbols until after
we've reviewed alternating current concepts in
Section~\ref{sec:alternating-current}.


\subsubsection{Types of Circuit Diagram}

There are really two kinds of circuit diagram, which are somewhat
different, they can describe:

\begin{itemize}
  \item a physical assembly of real-world non-ideal components for assembly/production; and
  \item a theoretical assembly of ideal components for analysis/simulation.
\end{itemize}

The example of Figure~\ref{fig:flashlight} shows a ``production''
schematic for a flashlight: the positive terminal of a 3~\unit{V}
battery is connected via a latching switch to a lamp, the other
terminal of which is connected to the chassis. The battery's negative
terminal is also connected to the chassis, completing the circuit.

Figure~\ref{fig:flashlight} also shows an ``analysis'' schematic for
the same flashlight --- admittedly an outlandish pedagogical
contrivance --- but this time the battery is modeled as a 3~\unit{V}
independent voltage source in series with a resistance, $R_B$,
representing the internal resistance of the battery. The switch is
generic (for analysis purposes, we don't care if it latches or not),
the negative terminal of the battery is connected explicitly to the
lamp, and that node is connected to ground.

\begin{figure}[H]
  \centering
  \begin{subfigure}{\twowide}
    \centering
    \begin{circuitikz}
      \draw (0,0) node[cground] {};
      \draw (0,0) to [battery,v=$3~\unit{V}$] (0,2);
      \draw (0,2) to [lspst] (2,2);
      \draw (2,2) to [lamp] (2,0) node[cground] {};
    \end{circuitikz}
    \caption{Production}
  \end{subfigure}
  \begin{subfigure}{\twowide}
    \centering
    \begin{circuitikz}
      \draw (0,0) node[ground] {};
      \draw (0,0) to [V=$3~\unit{V}$] (0,2);
      \draw (0,2) to [R=$R_\mathrm{B}$] (2,2);
      \draw (2,2) to [spst] (4,2);
      \draw (4,2) to [lamp] (4,0);
      \draw (4,0) to [short] (0,0);
    \end{circuitikz}
    \caption{Analysis}
  \end{subfigure}
  \caption{Production and Analysis Schematics for a Flashlight}
  \label{fig:flashlight}
\end{figure}
